#!usr/bin/python3

#===============================================================================
# Filter_BLAST.py
#===============================================================================

#Yves BAWIN July 2021
#Python script for filtering output file of BLAST.
#
#===============================================================================
# Import modules
#===============================================================================

import os, argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Filter BLAST output file.')

'''Mandatory arguments.'''
parser.add_argument('-f', '--file',
                    type = str,
                    help = 'Name of the BLAST output file in the input directory.')

'''Input data options.'''
parser.add_argument('-i', '--input_directory',
                    type = str,
                    default = '.',
                    help = 'Input directory (default = current directory).')

'''Analysis options.'''
parser.add_argument('--min_percentage_identity',
                    type = float,
                    default = 95,
                    help = 'Minimum percentage identity of a filtered BLAST hit (default = 95%).')
parser.add_argument('--max_Evalue',
                    type = float,
                    default = 0.0001,
                    help = 'Maximum Evalue of a filtered BLAST hit (default = 1e-4).')
parser.add_argument('--max_mismatches',
                    type = int,
                    default = 5,
                    help = 'Maximum number of mismatches between a filtered BLAST hit and the subject sequence (default = 5).')
parser.add_argument('--max_gaps',
                    type = int,
                    default = 1,
                    help = 'Maximum number of gaps in a filtered BLAST hit compared to the subject sequence (default = 1).')
parser.add_argument('--min_perc_alignment_length',
                    type = float,
                    default = 95,
                    help = 'Minimum percentage of the alignment length of a BLAST hit to the subject sequence compared to the query sequence length (default = 95%).')

'''Output data options.'''
parser.add_argument('-o', '--output_directory',
                    type = str,
                    default = '.',
                    help = 'Output directory (default = current directory).')
parser.add_argument('-s', '--suffix',
                    type = str,
                    default = 'filtered',
                    help = 'Suffix added to output files (default = filtered).')
parser.add_argument('--unique_BLAST_hits',
                    dest = 'unique',
                    action = 'store_true',
                    help = 'Print loci with only one BLAST hit to an additional tab-delimited text file  (default = no file with unique BLAST hits created).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    print('-------------------')
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    print('-------------------\n')
    return

'''Conversion functions.'''
def txt2dict(file = args['file'], in_d = args['input_directory']):
    txt_dict = dict()
    for line in open('{}/{}'.format(in_d, file)):
        line = line.rstrip().split('\t')
        start, stop = line[0].split(':')[1].split('-')
        length = int(stop) - int(start) + 1
        if line[0] not in txt_dict:
            txt_dict[line[0]] = [[length] + line[1:]]
        else:
            txt_dict[line[0]].append([length] + line[1:])
    return txt_dict

#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    print_date()
    
    #Convert BLAST output file into a dictionary.
    BLAST_dict = txt2dict()
    
    #Filter BLAST output file.
    print(' Filtering BLAST hits for a minimum percentage identity of {}%, a maximum E-value of {}, a maximum number of mismatches of {}, a maximum number of gaps of {}, and a minimum percentage of the aligment length of {}% ...'.format(args['min_percentage_identity'], args['max_mismatches'], args['max_gaps'], args['max_Evalue'], args['min_perc_alignment_length']))
    filtered_BLAST_dict = dict()
    for id, hits in BLAST_dict.items():
        for hit in hits:
            #Filter for a minimum percentage identity.
            if float(hit[2]) >= args['min_percentage_identity']:
                #Filter for a maximum number of mismatches.
                if int(hit[4]) <= args['max_mismatches']:
                    #Filter for a maximum number of gaps.
                    if int(hit[5]) <= args['max_gaps']:
                        #Filter for a maximum Evalue.
                        if float(hit[10]) <= args['max_Evalue']:
                            #Filter for a minimum percentage alignment length.
                            if int(hit[3]) / hit[0] * 100 >= args['min_perc_alignment_length']:
                                if id not in filtered_BLAST_dict:
                                    filtered_BLAST_dict[id] = [hit[1:]]
                                else:
                                    filtered_BLAST_dict[id].append(hit[1:])
                                    
    #Count the number of BLAST hits with a unique match.
    unique_hits = 0
    name = os.path.splitext(args['file'])[0]
    out = open('{}/{}_{}.txt'.format(args['output_directory'], name, args['suffix']), 'w+')
    print('Query sequence ID\tSubject sequence ID\tPercentage Identity\tAlignment length\tNumber of mismatches\tNumber of gaps\tQuery start\tQuery end\tSubject start\tSubject end\tE-value\tBitscore', file = out)
    if args['unique']:
        out_unique = open('{}/{}_{}_unique.txt'.format(args['output_directory'], name, args['suffix']), 'w+')
        print('Query sequence ID\tSubject sequence ID\tPercentage Identity\tAlignment length\tNumber of mismatches\tNumber of gaps\tQuery start\tQuery end\tSubject start\tSubject end\tE-value\tBitscore', file = out_unique)
    for id, hits in filtered_BLAST_dict.items():
        for hit in hits:
            print('{}\t{}'.format(id, '\t'.join(hit)), file = out)
            if args['unique'] and len(hits) == 1:
                unique_hits += 1
                print('{}\t{}'.format(id, '\t'.join(hit)), file = out_unique)
    print(' * {} out of {} regions ({}%) were retained after filtering.'.format(len(filtered_BLAST_dict), len(BLAST_dict), round(len(filtered_BLAST_dict) / len(BLAST_dict) * 100, 2) if len(filtered_BLAST_dict) > 0 else 0))
    if args['unique']:
        print(' * {} out of {} retained regions ({}%) had only one BLAST hit.'.format(unique_hits, len(filtered_BLAST_dict), round(unique_hits / len(filtered_BLAST_dict) * 100, 2) if len(filtered_BLAST_dict) > 0 else 0))
    print(' * Finished!\n')
    print_date()
