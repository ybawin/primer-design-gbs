#!usr/bin/python3

#===============================================================================
# Filter_PrimerDesignOutput.py
#===============================================================================

#Yves BAWIN July 2021
#Python script to filter the primer design output files based on a list of locus IDs.
#
#===============================================================================
# Import modules
#===============================================================================

import argparse
from datetime import datetime

#===============================================================================
# Parse arguments
#===============================================================================

#Create an ArgumentParser object
parser = argparse.ArgumentParser(description = 'Filter output files of the primer design script based on a list of locus IDs.')

'''Mandatory arguments.'''
parser.add_argument('-f', '--file',
                    type = str,
                    help = 'Directory and name of a tab-delimited text file containing a list of the primer design loci in the first column (e.g. the BLAST output file).')

'''Input data options.'''
parser.add_argument('-l', '--label',
                    type = str,
                    default = 'set_1',
                    help = 'Label shared by the primer design output files (default = set_1).')
parser.add_argument('-i', '--input_directory',
                    type = str,
                    default = '.',
                    help = 'Input directory containing the primer design output files (default = current directory).')

'''Output data options.'''
parser.add_argument('-o', '--output_directory',
                    type = str,
                    default = '.',
                    help = 'Output directory (default = current directory).')
parser.add_argument('-s', '--suffix',
                    type = str,
                    default = 'filtered',
                    help = 'Suffix added to output files (default = filtered).')

#Parse arguments to a dictionary
args = vars(parser.parse_args())

#===============================================================================
# Functions
#===============================================================================
def print_date ():
    print('-------------------')
    print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    print('-------------------\n')
    return

def extract_BLAST_loci(f = args['file']):
    loci = set()
    blast_output = open(f)
    for line in blast_output:
        if not line.startswith('Query sequence ID'):
            line = line.rstrip().split('\t')
            loci.add(line[0])
    return loci

def check_txt(txt, loci, in_dir = args['input_directory'], out_dir = args['output_directory'], s = args['suffix']):
    out = open('{}/{}_{}.txt'.format(out_dir, txt, s), 'w+')
    txt = open('{}/{}.txt'.format(in_dir, txt))
    header = txt.readline()
    print(header, end = '', file = out)
    inserts = list()
    for index, line in enumerate(txt):
        primer = line.rstrip().split('\t')
        if primer[0][:-2] in loci:
            print(line, end = '', file = out)
            if index % 2 == 0:
                chrom, start = primer[0].split(':')[0], primer[3]
            else:
                end = primer[2]
                inserts.append('{}:{}-{}'.format(chrom, start, end))
    return inserts

def check_bed(bed, loci, modus = 'one_line', in_dir = args['input_directory'], out_dir = args['output_directory'], s = args['suffix']):
    out = open('{}/{}_{}.{}'.format(out_dir, bed, s, 'gff' if modus == 'two_lines_gff' else 'bed'), 'w+')
    bed = open('{}/{}.{}'.format(in_dir, bed, 'gff' if modus == 'two_lines_gff' else 'bed'))
    for index, line in enumerate(bed):
        complete = False
        l = line.rstrip().split('\t')
        if modus.startswith('two_lines'):
            if index % 2 == 0:
                chrom = l[0]
                if modus == 'two_lines_gff':
                    start = int(l[4])
                else:
                    start = int(l[1]) + 1
                first_line = line
            else:
                if modus == 'two_lines_gff':
                    end = int(l[3])
                else:
                    end = int(l[2])
                complete = True
        else:
            chrom, start, end = l[0], l[1], int(l[2]) + 1
            complete = True
        if complete and '{}:{}-{}'.format(chrom, start, end) in loci:
            if modus.startswith('two_lines'):
                print(first_line, end = '', file = out)
            print(line, end = '', file = out)
    return

def check_fasta(fasta, loci, modus = 'one_line', in_dir = args['input_directory'], out_dir = args['output_directory'], s = args['suffix']):
    out = open('{}/{}_{}.fasta'.format(out_dir, fasta, s), 'w+')
    fasta = open('{}/{}.fasta'.format(in_dir, fasta))
    for line in fasta:
        if line.startswith('>'):
            locus = line.rstrip()[1:]
            if locus in loci:
                print(line, end = '', file = out)
                line = fasta.readline()
                print(line, end = '', file = out)
    return
#===============================================================================
# Script
#===============================================================================

if __name__ == '__main__':
    print_date()
    
    #Extract loci from BLAST output file.
    loci = extract_BLAST_loci()
    label = args['label']
    
    #Filter the text file with primer sequences.
    print(' * Filtering the output files from primer design ...')
    inserts = check_txt('Primers_' + label, loci)
    
    #Filter the BED file with primer coordinates.
    check_bed('Primers_' + label, loci, 'two_lines')
    
    #Filter the FastA file with the amplicon sequences.
    check_fasta('Amplicons_' + label, loci)
    
    #Filter the SMAP input files.
    check_bed('SMAP_haplotype_sites_' + label, inserts)
    check_bed('SMAP_haplotype_window_' + label, inserts, modus = 'two_lines_gff')
    print(' * Finished!\n')
    
    print_date()
        
