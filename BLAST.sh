#!/bin/bash
# (c) Yves Bawin 2021
# This bash script is used to perform a nucleotide BLAST with BLAST+ on a set of loci in a FastA file.
# It consists of the following steps:
# Step 1: Create a BLAST database.
# Step 2: Perform a nucleotide BLAST
########################################################################################

date
###################
#PARAMETER SETTINGS
###################

#Name of the FastA file (.fasta).
INPUT_FASTA=

#Directory and name of the reference genome sequence used for nucleotide BLAST.
REFERENCE_GENOME=

#Directory containing the FastA file (without final slash).
INPUT_DIRECTORY=.

#Output directory for the BLAST output table (without final slash).
OUTPUT_DIRECTORY=.

#Number of threads used for the nucleotide BLAST.
THREADS=4

#Maximum number of hits.
MAX_HITS=10

#E-value for nucleotide BLAST.
E_VALUE=1e-4

#Step 1: Create BLAST database
echo "Creating BLAST database"
echo
NAME=`basename $INPUT_FASTA .fasta`
makeblastdb -in $REFERENCE_GENOME -dbtype nucl -out $OUTPUT_DIRECTORY/Blast_DB_"$NAME"

#Step 2: Conduct nucleotide BLAST.
echo "Conducting a nucleotide BLAST ..."
blastn -query $INPUT_DIRECTORY/$INPUT_FASTA -db $OUTPUT_DIRECTORY/Blast_DB_"$NAME" -out $OUTPUT_DIRECTORY/OutputBLAST_"$NAME"_trial.txt -outfmt '6 qseqid sseqid pident length mismatch gapopen qstart qend sstart send evalue bitscore' -evalue $E_VALUE -max_target_seqs $MAX_HITS -num_threads $THREADS -dust no
echo "Finished!"

date
