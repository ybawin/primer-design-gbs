# Primer design with Primer3

## Description

The scripts in this GitLab project were used to design primers based on high-throughput sequencing data and to process the output files of primer design. Four scripts are included:

1. **Primer_design.py** combines a reference genome sequence with single nucleotide polymorphisms (SNPs) called in high-througput sequencing data into template sequences and designs primers for each template sequence with Primer3.
2. **BLAST.sh** launches a nucleotide BLAST for genomic regions.
3. **Filter_BLAST.py** filters the BLAST output file from BLAST.sh.
4. **Filter_PrimerDesignOutput.py** filters the output files of primer design based on a list of locus IDs.

## Requirements

All python scripts were developed in python3.8.
Further requirements are listed, tagged per component: <sup>1</sup>Primer_design.py, <sup>2</sup>BLAST.sh, <sup>3</sup>Filter_BLAST.py, <sup>4</sup>Filter_PrimerDesignOutput.py.

python 3.8<sup>1,3,4</sup>, biopython<sup>1</sup>, natsort<sup>1</sup>, primer3-py<sup>1</sup>, pandas<sup>1</sup>, NCBI BLAST+<sup>2</sup>.

## Building and installing

### 1. Primer_design.py

The script Primer_design.py converts a reference genome sequence and single nucleotide polymorphisms (SNPs) into template sequences and designs primers for each template sequence with Primer3 (Untergasser *et al*., 2012). The script requires at least a reference genome sequence (in FastA format) and 
a VCF (Variant Call Format) file containing SNP positions (first column: contig ID, second column: genomic location of SNP). The different options of the script are illustrated by a simplified drawing of whole genome (shotgun) sequencing (WGS) data and/or genotyping-by-sequencing (GBS) data (**Figure 1**), but SNPs from other sequence data types (*e.g.* RNA-seq data, probe capture data) can be used as input for the script as well.

![Illustration of whole genome sequencing data (WGS, upper) and genotyping-by-sequencing data (GBS, lower).](/Readme_figures/Examples.PNG)
**Figure 1.** Graphical representation of sequencing reads (grey bars) containing SNPs (yellow squares) from whole genome sequencing libraries (WGS, upper) and genotyping-by-sequencing libraries (GBS, lower) that are mapped onto a reference genome sequence (black bar). These representations will be used throughout this document to demonstrate the different options of the primer design script. 

In the absence of other input files, SNPs located within a predefined distance from each other (defined by the --variant_distance option) are assigned to the same template sequence. A template sequence with only one SNP usually has a size equal to the variant distance with the SNP positioned in the centre of the template. For example, if the variant distance equals 500 base pairs (default), a template sequence with only one SNP will contain 500 nucleotides. The first 250 base pairs are equal to the reference genome sequence, followed by one SNP (N), and ending with another 249 base pairs equal to the reference genome sequence. If that one SNP is located less than half the variant distance from the beginning or end of the contig, the template sequence is shorter than the variant distance). In the previous example, the template sequence is shortened to 450 base pairs (instead of 500 base pairs) if the SNP lies on position 201. The sequence consists of 200 base pairs that are equal to the reference genome sequence followed by the SNP and another 249 base pairs equal to the reference genome sequence. If a second SNP would have been spaced 10 base pairs from a first SNP, the template sequence would have been extended with 10 base pairs resulting in the following template sequence: 200 base pairs equal to the reference genome sequence - the first SNP - 9 base pairs equal to the reference genome sequence - the second SNP - 249 base pairs equal to the reference genome sequence. Hence, as long as consecutive SNPs are positioned from each other within a distance equal to half the variant distance or less, they will be included in the same template sequence. In theory, the size of a template sequence can be as large as the size of its corresponding contig (**Figure 2**).

By default, each SNP in a template sequence is set as a target for primer design. However, SNPs that are positioned closely to each other (within a distance specified by the --target_size option) are grouped as one target in the template sequence. If the distance between the final nucleotide of a target and the next SNP is smaller than the value specified in the --target_distance option, that SNP will be skipped and not be included in any target.
In other words, targets within the same template will be spaced from each other by a minimum distance equal to the --target_distance. Primer3 will attempt to develop only one primer pair for each template sequence, even if a template has multiple targets (**Figure 2**).

![Primer design with only a VCF file and a reference genome sequence.](/Readme_figures/Primer_design_default.PNG)
**Figure 2.** Primer design using WGS (upper) and GBS (lower) data with a reference genome sequence and a VCF file as input. Template sequences contain at least one SNP, but can encompass multiple SNPs if the number of nucleotides between consecutive SNPs is smaller than the predefined maximum variant distance (*e.g.* template sequence 2 and 5 in the WGS example). Template sequences are also allowed to exceed the end coordinates of GBS stacks in this scenario. One or multiple SNPs are combined in the same target (orange bars) with a maximum size equal to the target size. Multiple targets in one template are positioned from each other by a minimum target distance. Amplicons (green bars) comprising one of the targets have a size between the predefined minimum size and maximum size. Only one amplicon is designed per template sequence.

The script can also take two other VCF files as input (**Figure 3**):

- A VCF file with target SNPs (--target_vcf) containing a user selection of SNPs for the delineation of targets. Only SNPs in this VCF will be considered as potential targets for primer design. Other SNP positions in the main VCF file (specified with the --vcf option) will be incorporated in the template sequences, but won't be set as targets.
- A VCF file with positions that differ *between* the reference sample set and the reference genome sequence. These positions are not polymorphic *within* the reference sample set, but they differ between every sample and the reference genome sequence. To adjust the reference genome sequence for these differences, the user can parse a VCF file with all called differences between the reference genome sequence and the reference samples to the script.
  However, these differences will not be treated as SNPs, as they are not polymorphic among samples. Hence, they won't be set as targets for primer design.

![Visualisation of target VCF and reference VCF option.](/Readme_figures/Target_reference_VCF.png)
**Figure 3.** Primer design with a target VCF (upper) or a reference VCF (lower) file. Only SNPs in the target VCF file (in black rectangles) are included in targets. The reference VCF file contains differences between the reference data set and the reference genome sequence (light orange squares) to adjust the template sequences for deviations of reference samples from the reference genome sequence.

The user can also choose to additionally parse a BED (Browser Extensible Data) file (http://genome.cse.ucsc.edu/FAQ/FAQformat.html#format1) with the genomic coordinates of template sequences for primer design to the script via the --regions option (**Figure 4**). Only these regions are then considered for primer design. The start coordinate of each region in the VCF file must be smaller than the end coordinate. SNPs in the VCF file(s) that lie outside the regions defined in the BED file are ignored. If the regions are fairly large (e.g. gene sequences), the user can split these regions into multiple templates using the --split_template option (**Figure 5**). The template sequences will be constructed using the same procedure as stated above without using the --regions option.

![Illustration of offset regions.](/Readme_figures/BED_regions.PNG)
**Figure 4.** Primer design using GBS data with a BED file containing genomic coordinates (lower) compared to primer design using GBS data without a BED file (upper). Template sequences are not allowed to exceed the genomic coordinates in the BED file (shown by the orange lines).

![Illustration of split_template option.](/Readme_figures/Split_template.PNG)
**Figure 5.** Primer design using WGS data and a BED file with the split_template option (lower) compared to primer design using WGS data and a BED file without the split_template option. The split template option splits the template sequence delineated in the BED file (orange lines) into multiple template sequences using the same reasoning as illustrated by figure 2.

Users can also opt to ignore targets that are close to the region borders by setting an offset (**Figure 6**). SNPs that are positioned within the offset are not considered as targets, but the positions of these variants are still marked in the template sequences to take them into account for primer design. By default, the script does not allow primer3 to design primers with unknown nucleotides (N) so that primer sequences never overlap with known SNP positions in the provided VCF file(s). If users want to design degenerated primers containing unknown nucleotides, the number of unknown nucleotides in primer sequences can be increased using the --maxn option.

![Illustration of offset regions.](/Readme_figures/Offset.png)
**Figure 6.** Primer design using GBS data and a BED file with offsets (lower) compared to primer design using GBS data and a BED file without offsets (upper). SNPs within offset regions (delineated by the green lines) are incorporated in the template sequences, but not included in target regions.

A user can choose to extend a region at the 5'-end and 3'-end with a number of base pairs using the --region_extension option (**Figure 7**). If this number is larger than 0 (default), the script adds the corresponding number of base pairs to both ends of each region in the BED file provided with the --regions option. If SNPs were called (present in the VCF file(s) parsed with the --vcf option and/or the --target_vcf option) in the added fragments, they will be added to the template sequence. Extending regions with a (small) number of base pairs can be advantageous for primer design, because the extended parts may provide more possibilities for primer3 to design primers around targets. Enlarging regions might be especially interesting for regions with a low SNP density, as it is less likely that new SNPs are added to the region borders by the extensions.

![Illustration of region extension.](/Readme_figures/Region_extension.png)
**Figure 7.** Primer design using GBS data and a BED file with region extension (lower) compared to primer design using GBS data and a BED file without region extension (upper). The template sequences are extended at the 5' and 3' end of the genomic coordinates in the BED file (orange lines) to the new region ends (green lines).

Extensions may cause an overlap between neighbouring regions, creating template sequences with the same genomic fragments. This may result in artificial mispriming (see below), inhibiting primer design for these templates. To avoid such artificial mispriming, the script checks by default (also if regions are not extended) for overlapping regions. Regions that partially overlap with another region located more upstream in the contig are trimmed to the most upstream nucleotide that does not overlap with another region. If regions fully overlap, the shortest region is discarded (**Figure 8**). If preferred, overlapping sequence parts can be retained using the --retain_overlap option.

![Different scenario's of region overlap.](/Readme_figures/Region_overlap.PNG)
**Figure 8.** Overview of the different scenario's of region overlap before (upper) and after (lower) the exclusion of overlap. Partially overlapping regions are trimmed to the first non-overlapping nucleotide in the 5'-end of the most downstream region, while the shortest region of completely overlapping regions is removed.

Furthermore, the minimum and maximum size of an amplicon (including the primer sequences) can be specified. It is recommended to keep the size range of amplicons relatively small (e.g. 10 or 20 base pairs). A small size interval avoids the relative overamplification of smaller amplicons compared to the larger ones in the same PCR. Regions delineated in the BED file (optionally) provided with the --regions option must be at least as large as the minimum amplicon size. Shorter regions are not processed during primer design.

The primer size range can also be adjusted using the options --minimum_primer_size, --optimal_primer_size, and --maximum_primer_size. The specified optimal size of a primer must always be larger than or equal to the minimum primer size and smaller than or equal to the maximum primer size. If not, the script will adjust the optimal primer size to the value that is closest to the defined primer size range (i.e. the minimum or maximum primer size if the optimal primer size was smaller than the minimum or larger as the maximum, respectively). The script also checks for mispriming (i.e. the annealing of primer sequences to (partially) complementary sequences), both within the same template sequence and in the other template sequences. Parts of the reference genome sequence that are not included in a template sequence are not checked for mispriming.

The primer sequences are outputted in a tab-delimited text file in table format containing the amplicon ID (contig_ID:start-end), the orientation of the primer (forward or reverse), the genomic position of the first nucleotide in the primer sequence (start), the genomic position of the last nucleotide in the primer sequence (stop), and the nucleotide sequence (5' to 3'). The start and end coordinates in the amplicon ID correspond to the first nucleotide downstream of the 3'-end of the forward primer and the last nucleotide upstream of the 3'-end of the reverse primer. In addition, the primer coordinates are stored in a BED file (0-based, start coordinate inclusive, stop coordinate exclusive), and a BED and GFF input file is created for SMAP haplotype-sites and SMAP haplotype-windows, respectively. The coordinates of the extended amplicons (including primer regions) are also provided in a BED output file. The template sequences (including all modifications to the reference genome sequence) and the reference genome sequences of the amplicons (e.g. to perform a nucleotide BLAST) are also provided in FastA format. All output files are saved in the specified output directory.

#### Usage

`python3 Primer_design.py --vcf VCF --reference REFERENCE [-i INPUT_DIRECTORY] [-r REGIONS] [--target_vcf TARGET_VCF] [--reference_vcf REFERENCE_VCF] [-d VARIANT_DISTANCE] [-t TARGET_SIZE] [-u TARGET_DISTANCE] [-min MINIMUM_AMPLICON_SIZE] [-max MAXIMUM_AMPLICON_SIZE] [--offset OFFSET] [-minp MINIMUM_PRIMER_SIZE] [-maxp MAXIMUM_PRIMER_SIZE] [-optp OPTIMAL_PRIMER_SIZE] [-maxmis MAXIMUM_MISPRIMING] [-max_n MAXIMUM_UNKNOWN_NUCLEOTIDES] [-ex REGION_EXTENSION] [--retain_overlap] [--split_region] [-b BORDER_SIZE] [-o OUTPUT_DIRECTORY] [-s SUFFIX]`

**Mandatory arguments**

    --vcf        STR  Name of the VCF file in the input directory with SNPs.
    --reference  STR  Directory and name of the reference genome sequence (in FastA format).

**Input data options**

    -i, --input_directory  STR  Input directory [current directory].
    -r, --regions          STR  Name of the BED file in the input directory containing the genomic coordinates of regions wherein primers must be designed [no BED file provided].
    --target_vcf           STR  Name of the VCF file in the input directory containing target SNPs [no VCF file with target SNPs provided].
    --reference_vcf        STR  Name of the VCF file in the input directory containing non-polymorphic differences between the reference genome sequence and the samples for primer design [no VCF file with reference genome differences provided].

**Analysis options**

    -d, --variant_distance                 INT  Maximum distance (in base pairs) between two variants to be included in the same template [500].
    -t, --target_size                      INT  Maximum size (in base pairs) of a target region [10].
    -u, --target_distance                  INT  Minimum distance (in base pairs) between two target regions in a template [0].
    -min, --minimum_amplicon_size          INT  Minimum size of an amplicon (including primer sequences) in base pairs [100].
    -max, --maximum_amplicon_size          INT  Maximum size of an amplicon (including primer sequences) in base pairs [120].
    --offset                               INT  Size of the offset at the 5' and 3' end of each region. Variants in the offsets are not tagged as targets for primer design [0, all variants are potential targets].
    -minp, --minimum_primer_size           INT  Minimum size (in base pairs) of a primer [18].
    -maxp, --maximum_primer_size           INT  Maximum size (in base pairs) of a primer [27].
    -optp, --optimal_primer_size           INT  Optimal size (in base pairs) of a primer [20].
    -max_mis, --maximum_mispriming         INT  Maximum weighted similarity of a primer with the same template and other templates [12].
    -maxn, --maximum_unknown_nucleotides  INT  Maximum number of unknown nucleotides (N) in a primer sequence [0].
    -ex, --region_extension                INT  Extend regions in the BED file provided via the --regions option at their 5' end 3' end with the specified value [0, no region extension].
    --retain_overlap                            Retain overlap in template sequences among regions [overlap in template sequences is removed].
    --split_region                              Split the regions in the BED file provided via the --regions options in multiple templates based on the variant_distance [regions are not split].

**Output data options**

    -b, --border_size       INT  Border size used in the input file of SMAP haplotype-window [10].
    -o, --output_directory  STR  Output directory [current directory].
    -s, --suffix            STR  Suffix added to output files [set_1].


### 2. BLAST.sh

The script BLAST.sh launches a nucleotide BLAST with BLAST+ (Camacho *et al*., 2009) for genomic regions in FastA format. The FastA file with amplicon regions can be retrieved from the Primer_design.py script. The script creates a BLAST database in the output directory and a tab-delimited text file with the BLAST output data in table format. The output data are ordered in the following columns (from left to right):

    - qseqid: query sequence ID
    - sseqid: subject sequence ID
    - pident: percentage identity
    - length: alignment length
    - mismatch: number of mismatches
    - gapopen: number of gaps
    - qstart: query start
    - qend: query end
    - sstart: subject start
    - send: subject end
    - evalue: E-value
    - bitscore: Bitscore

#### Usage

`bash BLAST.sh`

**Mandatory arguments**

    INPUT_FASTA       Name of the FastA file (.fasta).
    REFERENCE_GENOME  Directory and name of the reference genome sequence used for nucleotide BLAST.

**Input data options**

    INPUT_DIRECTORY   Directory containing the FastA file (without final slash) [current directory].

**Analysis options**

    THREADS           Number of threads used for the nucleotide BLAST [4].
    MAX_HITS          Maximum number of hits [10].
    E_VALUE           E-value for nucleotide BLAST [1e-4].

**Output data options**

    OUTPUT_DIRECTORY  Output directory for the BLAST output table (without final slash) [current directory].

### 3. Filter_BLAST.py

The script Filter_BLAST.py filters the BLAST output file from BLAST.sh. The BLAST output files can be filtered for a minimum percentage identity, a maximum E-value, a maximum number of mismatches, a maximum number of gaps, and a minimum percentage of alignment length of the subject (database sequence) compared to the query (own sequence).
The results are printed in table format to a tab-delimited text file in the output directory. That table is aslo supplemented with a header. In addition, the user can print all regions with only one BLAST hit to a separate tab-delimited text file by using the --unique_BLAST_hits option.

#### Usage

`python3 Filter_BLAST.py -f FILE [-i INPUT_DIRECTORY] [--min_percentage_identity MIN_PERCENTAGE_IDENTITY] [--max_Evalue MAX_EVALUE] [--max_mismatches MAX_MISMATCHES] [--max_gaps MAX_GAPS] [--min_perc_alignment_length MIN_PERC_ALIGNMENT_LENGTH] [-o OUTPUT_DIRECTORY] [-s SUFFIX] [--unique_BLAST_hits]`

**Mandatory arguments**

    -f, --file  STR  Name of the BLAST output file in the input directory.

**Input data options**

    -i, --input_directory  STR  Input directory [current directory].

**Analysis options**

    --min_percentage_identity    FLOAT  Minimum percentage identity of a filtered BLAST hit [95%].
    --max_Evalue                 FLOAT  Maximum Evalue of a filtered BLAST hit [1e-4].
    --max_mismatches             INT    Maximum number of mismatches between a filtered BLAST hit and the subject sequence [5].
    --max_gaps                   INT    Maximum number of gaps in a filtered BLAST hit compared to the subject sequence [1].
    --min_perc_alignment_length  FLOAT  Minimum percentage of the alignment length of a BLAST hit to the subject sequence compared to the query sequence length [95%].

**Output data options**

    -o, --output_directory  STR  Output directory [current directory].
    -s, --suffix            STR  Suffix added to output files [filtered].
    --unique_BLAST_hits          Print loci with only one BLAST hit to an additional tab-delimited text file [no file with unique BLAST hits created].

### 4. Filter_PrimerDesignOutput.py

The script Filter_PrimerDesignOutput.py filters the following files created with the Primer_design.py script based on a list of locus IDs:

- The text file with primer sequences (file name starting with 'Primers')
- The BED file with primer coordinates (file name starting with 'Primers')
- The FastA file with the amplicon sequences (file name starting with 'Amplicons')
- The input BED file of SMAP haplotype-sites (file name starting with 'SMAP_haplotype_sites')
- The input GFF file of SMAP haplotype-window (file name starting with 'SMAP_haplotype_window')

The locus IDs must be listed in the first column of a tab-delimited text file. The (filtered) tab-delimited text file created by the BLAST.sh script or the Filter_BLAST.py script can be parsed to the script without any modifications. A new, filtered file with the user-specified suffix is created for each of the primer design output files listed above.

#### Usage

`python3 Filter_PrimerDesignOutput.py -f FILE [-l LABEL] [-i INPUT_DIRECTORY] [-o OUTPUT_DIRECTORY] [-s SUFFIX]`

**Mandatory argument**

    -f, --file  STR  Directory and name of a tab-delimited text file containing a list of the primer design loci in the first column (e.g. the BLAST output file).

**Input data options**

    -l, --label            STR  Label shared by the primer design output files [set_1].
    -i, --input_directory  STR  Input directory containing the primer design output files [current directory].

**Output data options**

    -o, --output_directory  STR  Output directory [current directory].
    -s, --suffix            STR  Suffix added to output files [filtered].

#### References
Camacho *et al*., 2009. BLAST+: architecture and applications. *BMC Bioinformatics, 10*, 421. https://doi.org/10.1186/1471-2105-10-421

Untergasser *et al*., 2012. Primer3—new capabilities and interfaces. *Nucleic Acids Research, 40*(15), e115. https://doi.org/10.1093/nar/gks596
